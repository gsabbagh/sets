# Revision history for Sets

## 0.1.0.0 -- 2022-07-12

* First version. Released on an unsuspecting world.

## 1.0.0.0 -- 2022-07-17

* Data.WeakSet and Data.WeakMap are now valid alternatives to Data.Set and Data.Map.

# 1.1.0.0 -- 2022-07-19

* Correction of wrong function declaration of (|^|)

# 1.1.1.0 -- 2022-07-19

* Implementation of drop, take and splitAt

# 1.1.2.0 -- 2022-07-19

* Addition of weakMapFromSet and correction of WeakMap Show instance.

# 1.1.3.0 -- 2022-07-20

* Addition of inverse and pseudoInverse functions.

# 1.1.4.0 -- 2022-07-20

* Major bug correction : instance Eq for Data.WeakMap

# 1.2.0.0 -- 2022-07-20

* Major bug correction : instance Foldable removed for Data.WeakMap and Data.WeakSet

# 1.2.1.0 -- 2022-07-23

* Adding anElement function

# 1.2.2.0 -- 2022-07-24

* Adding enumerateMaps function

# 1.2.3.0 -- 2022-07-25

* Adding all functions from Foldable

# 1.2.4.0 -- 2022-07-26

* Adding Alternative instance

# 1.3.0.0 -- 2022-07-28

* Correction of cartesianProductOfSet

# 1.3.1.0 -- 2022-08-29

* Addition of first and second functions for PureSet.

# 1.3.2.0 -- 2022-08-30

* Bug correction in formatPureSet.

# 1.3.3.0 -- 2022-08-30

* Bug correction in pairs.

# 1.4.0.0 -- 2022-08-30

* Bug correction in maybeSecond.

# 1.4.0.1 -- 2023-09-23

* Base update to 4.18.0.0

# 1.5.0.0 -- 2023-10-10

* Better bounds

# 1.6.0.0 -- 2023-11-03

* Removing PureSet as they are useless. MonadPlus and MonadFail for Set. Simplifiable typeclass to remove duplicates in Set and Map.

# 1.6.1.0 -- 2023-11-05

* Adding Generic derivation of Simplifiable typeclass.