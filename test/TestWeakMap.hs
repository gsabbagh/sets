module TestWeakMap where
    import Data.WeakMap.Safe
    import qualified Data.WeakMap as M
    import Data.WeakSet.Safe
    import qualified Data.WeakSet as S
    
    -- | Tests all functions related to weak maps.
    main :: IO ()
    main = do
            putStrLn "Beginning of TestWeakMap"
            let s1 = set [1,2,3] 
            let s2 = set [3,4,5] 
            let f = weakMap $ zip (setToList s1) (setToList s2)
            let g = weakMap $ zip (setToList s2) (setToList s1)
            putStrLn $ show $ f
            putStrLn $ show $ g
            putStrLn $ show $ M.mapToSet $ f
            putStrLn $ show $ domain $ f
            putStrLn $ show $ image $ f
            putStrLn $ show $ f |?| 1
            putStrLn $ show $ f |!| 1
            putStrLn $ show $ f |.| g
            putStrLn $ show $ g |.| f
            putStrLn $ show $ memorizeFunction (*3) (set [1..10])
            putStrLn $ show $ idFromSet (set [1..10])
            putStrLn $ show $ M.size (idFromSet (set [1..10]))
            putStrLn $ show $ M.delete 3 (idFromSet (set [1..10]))
            putStrLn $ show $ M.adjust (+1) 3 (idFromSet (set [1..10]))
            putStrLn $ show $ M.adjust (+1) 13 (idFromSet (set [1..10]))
            putStrLn $ show $ M.union (idFromSet (set [1..10])) (idFromSet (set [5..15])) 
            putStrLn "End of TestWeakMap"
    