module Main (main) where
    import qualified TestWeakSet                as WSET
    import qualified TestWeakMap                as WMAP

    main :: IO ()
    main = do
        putStrLn "Beginning of the test."
        WSET.main
        WMAP.main
        putStrLn "End of the tests."