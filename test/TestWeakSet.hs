module TestWeakSet where
    import Data.WeakSet
    
    -- | Tests all functions related to homogeneous sets.
    main :: IO ()
    main = do
            putStrLn "Beginning of TestWeakSet"
            let s1 = set [1,2,3] 
            let s2 = set [3,4,5] 
            putStrLn $ show $ s1
            putStrLn $ show $ setToList s1
            putStrLn $ show $ s2 `isIncludedIn` s1
            putStrLn $ show $ (set [1,2]) `isIncludedIn` s1
            putStrLn $ show $ cardinal s1
            putStrLn $ show $ 3 `isIn` s1
            putStrLn $ show $ (+1) <$> s1
            putStrLn $ show $ (+) <$> s1 <*> s2
            putStrLn $ show $ s1 |&| s2
            putStrLn $ show $ s1 ||| s2
            putStrLn $ show $ s1 |*| s2
            putStrLn $ show $ s1 |+| s2
            putStrLn $ show $ s1 |-| s2
            putStrLn $ show $ s1 |^| 3
            putStrLn $ show $ powerSet s1
            putStrLn "End of TestWeakSet"
    