{-| Module  : WeakSets
Description : Non-clashing functions for `Set`s.
Copyright   : Guillaume Sabbagh 2022
License     : LGPL-3.0-or-later
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Non-clashing functions for `Set`s.
-}

module Data.WeakSet.Safe
(
    set, -- the smart constructor for `Set`
    -- * Set related functions
    setToList,
    isIncludedIn,
    cardinal,
    isIn,
    (|&|),
    (|||),
    (|*|),
    (|+|),
    (|-|),
    (|^|),
    nubSetBy,
    anElement,
    cartesianProductOfSets,
)
where
    import              Data.WeakSet
    