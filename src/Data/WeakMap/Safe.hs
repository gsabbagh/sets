{-| Module  : WeakSets
Description : Non-clashing functions for `Map`s.
Copyright   : Guillaume Sabbagh 2022
License     : LGPL-3.0-or-later
Maintainer  : guillaumesabbagh@protonmail.com
Stability   : experimental
Portability : portable

Non-clashing functions for `Map`s.
-}

module Data.WeakMap.Safe
(
    weakMap -- the smart constructor for `Map`
    ,  weakMapFromSet
    , (|?|)
    , (|!|)
    , (|.|)
    , idFromSet
    , memorizeFunction
    , elems'
    , keys'
    , domain
    , image
    , inverse
    , pseudoInverse
    , enumerateMaps
    , mapKeysAndValues
    , (<|$|>)
)
where
    import              Data.WeakMap
    